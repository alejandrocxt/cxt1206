; ModuleID = 'swap1.c'
target datalayout = "e-m:e-p:32:32-f64:32:64-f80:32-n8:16:32-S128"
target triple = "i686-pc-linux-gnu"

%struct.args = type { i32, i32, i32, %struct.buffer*, %union.pthread_mutex_t* }
%struct.buffer = type { i32*, i32 }
%union.pthread_mutex_t = type { %struct.__pthread_mutex_s }
%struct.__pthread_mutex_s = type { i32, i32, i32, i32, i32, %union.anon }
%union.anon = type { %struct.__pthread_internal_slist }
%struct.__pthread_internal_slist = type { %struct.__pthread_internal_slist* }
%struct.options = type { i32, i32, i32, i32 }
%struct.thread_info = type { i32, i32 }
%union.pthread_mutexattr_t = type { i32 }
%union.pthread_attr_t = type { i32, [32 x i8] }

@.str = private unnamed_addr constant [4 x i8] c"%i \00", align 1
@.str.1 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.2 = private unnamed_addr constant [19 x i8] c"Not enough memory\0A\00", align 1
@.str.3 = private unnamed_addr constant [21 x i8] c"creating %d threads\0A\00", align 1
@.str.4 = private unnamed_addr constant [16 x i8] c"Buffer before: \00", align 1
@.str.5 = private unnamed_addr constant [27 x i8] c"Failing creating thread %d\00", align 1
@.str.6 = private unnamed_addr constant [16 x i8] c"Buffer after:  \00", align 1

; Function Attrs: nounwind
define i8* @swap(i8* %ptr) #0 {
  %1 = alloca i8*, align 4
  %args = alloca %struct.args*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %tmp = alloca i32, align 4
  store i8* %ptr, i8** %1, align 4
  %2 = load i8*, i8** %1, align 4
  %3 = bitcast i8* %2 to %struct.args*
  store %struct.args* %3, %struct.args** %args, align 4
  br label %4

; <label>:4                                       ; preds = %89, %0
  %5 = load %struct.args*, %struct.args** %args, align 4
  %6 = getelementptr inbounds %struct.args, %struct.args* %5, i32 0, i32 2
  %7 = load i32, i32* %6, align 4
  %8 = add nsw i32 %7, -1
  store i32 %8, i32* %6, align 4
  %9 = icmp ne i32 %7, 0
  br i1 %9, label %10, label %94

; <label>:10                                      ; preds = %4
  %11 = load %struct.args*, %struct.args** %args, align 4
  %12 = getelementptr inbounds %struct.args, %struct.args* %11, i32 0, i32 4
  %13 = load %union.pthread_mutex_t*, %union.pthread_mutex_t** %12, align 4
  %14 = call i32 @pthread_mutex_lock(%union.pthread_mutex_t* %13) #4
  %15 = call i32 @rand() #4
  %16 = load %struct.args*, %struct.args** %args, align 4
  %17 = getelementptr inbounds %struct.args, %struct.args* %16, i32 0, i32 3
  %18 = load %struct.buffer*, %struct.buffer** %17, align 4
  %19 = getelementptr inbounds %struct.buffer, %struct.buffer* %18, i32 0, i32 1
  %20 = load i32, i32* %19, align 4
  %21 = srem i32 %15, %20
  store i32 %21, i32* %i, align 4
  %22 = call i32 @rand() #4
  %23 = load %struct.args*, %struct.args** %args, align 4
  %24 = getelementptr inbounds %struct.args, %struct.args* %23, i32 0, i32 3
  %25 = load %struct.buffer*, %struct.buffer** %24, align 4
  %26 = getelementptr inbounds %struct.buffer, %struct.buffer* %25, i32 0, i32 1
  %27 = load i32, i32* %26, align 4
  %28 = srem i32 %22, %27
  store i32 %28, i32* %j, align 4
  %29 = load i32, i32* %i, align 4
  %30 = load %struct.args*, %struct.args** %args, align 4
  %31 = getelementptr inbounds %struct.args, %struct.args* %30, i32 0, i32 3
  %32 = load %struct.buffer*, %struct.buffer** %31, align 4
  %33 = getelementptr inbounds %struct.buffer, %struct.buffer* %32, i32 0, i32 0
  %34 = load i32*, i32** %33, align 4
  %35 = getelementptr inbounds i32, i32* %34, i32 %29
  %36 = load i32, i32* %35, align 4
  store i32 %36, i32* %tmp, align 4
  %37 = load %struct.args*, %struct.args** %args, align 4
  %38 = getelementptr inbounds %struct.args, %struct.args* %37, i32 0, i32 1
  %39 = load i32, i32* %38, align 4
  %40 = icmp ne i32 %39, 0
  br i1 %40, label %41, label %46

; <label>:41                                      ; preds = %10
  %42 = load %struct.args*, %struct.args** %args, align 4
  %43 = getelementptr inbounds %struct.args, %struct.args* %42, i32 0, i32 1
  %44 = load i32, i32* %43, align 4
  %45 = call i32 @usleep(i32 %44)
  br label %46

; <label>:46                                      ; preds = %41, %10
  %47 = load i32, i32* %j, align 4
  %48 = load %struct.args*, %struct.args** %args, align 4
  %49 = getelementptr inbounds %struct.args, %struct.args* %48, i32 0, i32 3
  %50 = load %struct.buffer*, %struct.buffer** %49, align 4
  %51 = getelementptr inbounds %struct.buffer, %struct.buffer* %50, i32 0, i32 0
  %52 = load i32*, i32** %51, align 4
  %53 = getelementptr inbounds i32, i32* %52, i32 %47
  %54 = load i32, i32* %53, align 4
  %55 = load i32, i32* %i, align 4
  %56 = load %struct.args*, %struct.args** %args, align 4
  %57 = getelementptr inbounds %struct.args, %struct.args* %56, i32 0, i32 3
  %58 = load %struct.buffer*, %struct.buffer** %57, align 4
  %59 = getelementptr inbounds %struct.buffer, %struct.buffer* %58, i32 0, i32 0
  %60 = load i32*, i32** %59, align 4
  %61 = getelementptr inbounds i32, i32* %60, i32 %55
  store i32 %54, i32* %61, align 4
  %62 = load %struct.args*, %struct.args** %args, align 4
  %63 = getelementptr inbounds %struct.args, %struct.args* %62, i32 0, i32 1
  %64 = load i32, i32* %63, align 4
  %65 = icmp ne i32 %64, 0
  br i1 %65, label %66, label %71

; <label>:66                                      ; preds = %46
  %67 = load %struct.args*, %struct.args** %args, align 4
  %68 = getelementptr inbounds %struct.args, %struct.args* %67, i32 0, i32 1
  %69 = load i32, i32* %68, align 4
  %70 = call i32 @usleep(i32 %69)
  br label %71

; <label>:71                                      ; preds = %66, %46
  %72 = load i32, i32* %tmp, align 4
  %73 = load i32, i32* %j, align 4
  %74 = load %struct.args*, %struct.args** %args, align 4
  %75 = getelementptr inbounds %struct.args, %struct.args* %74, i32 0, i32 3
  %76 = load %struct.buffer*, %struct.buffer** %75, align 4
  %77 = getelementptr inbounds %struct.buffer, %struct.buffer* %76, i32 0, i32 0
  %78 = load i32*, i32** %77, align 4
  %79 = getelementptr inbounds i32, i32* %78, i32 %73
  store i32 %72, i32* %79, align 4
  %80 = load %struct.args*, %struct.args** %args, align 4
  %81 = getelementptr inbounds %struct.args, %struct.args* %80, i32 0, i32 1
  %82 = load i32, i32* %81, align 4
  %83 = icmp ne i32 %82, 0
  br i1 %83, label %84, label %89

; <label>:84                                      ; preds = %71
  %85 = load %struct.args*, %struct.args** %args, align 4
  %86 = getelementptr inbounds %struct.args, %struct.args* %85, i32 0, i32 1
  %87 = load i32, i32* %86, align 4
  %88 = call i32 @usleep(i32 %87)
  br label %89

; <label>:89                                      ; preds = %84, %71
  %90 = load %struct.args*, %struct.args** %args, align 4
  %91 = getelementptr inbounds %struct.args, %struct.args* %90, i32 0, i32 4
  %92 = load %union.pthread_mutex_t*, %union.pthread_mutex_t** %91, align 4
  %93 = call i32 @pthread_mutex_unlock(%union.pthread_mutex_t* %92) #4
  br label %4

; <label>:94                                      ; preds = %4
  ret i8* null
}

; Function Attrs: nounwind
declare i32 @pthread_mutex_lock(%union.pthread_mutex_t*) #0

; Function Attrs: nounwind
declare i32 @rand() #0

declare i32 @usleep(i32) #1

; Function Attrs: nounwind
declare i32 @pthread_mutex_unlock(%union.pthread_mutex_t*) #0

; Function Attrs: nounwind
define void @print_buffer(i32* %buffer.0, i32 %buffer.1) #0 {
  %buffer = alloca %struct.buffer, align 4
  %i = alloca i32, align 4
  %1 = getelementptr inbounds %struct.buffer, %struct.buffer* %buffer, i32 0, i32 0
  store i32* %buffer.0, i32** %1, align 4
  %2 = getelementptr inbounds %struct.buffer, %struct.buffer* %buffer, i32 0, i32 1
  store i32 %buffer.1, i32* %2, align 4
  store i32 0, i32* %i, align 4
  br label %3

; <label>:3                                       ; preds = %15, %0
  %4 = load i32, i32* %i, align 4
  %5 = getelementptr inbounds %struct.buffer, %struct.buffer* %buffer, i32 0, i32 1
  %6 = load i32, i32* %5, align 4
  %7 = icmp slt i32 %4, %6
  br i1 %7, label %8, label %18

; <label>:8                                       ; preds = %3
  %9 = load i32, i32* %i, align 4
  %10 = getelementptr inbounds %struct.buffer, %struct.buffer* %buffer, i32 0, i32 0
  %11 = load i32*, i32** %10, align 4
  %12 = getelementptr inbounds i32, i32* %11, i32 %9
  %13 = load i32, i32* %12, align 4
  %14 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0), i32 %13)
  br label %15

; <label>:15                                      ; preds = %8
  %16 = load i32, i32* %i, align 4
  %17 = add nsw i32 %16, 1
  store i32 %17, i32* %i, align 4
  br label %3

; <label>:18                                      ; preds = %3
  %19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0))
  ret void
}

declare i32 @printf(i8*, ...) #1

; Function Attrs: nounwind
define void @start_threads(i32 %opt.0, i32 %opt.1, i32 %opt.2, i32 %opt.3) #0 {
  %opt = alloca %struct.options, align 4
  %i = alloca i32, align 4
  %threads = alloca %struct.thread_info*, align 4
  %args = alloca %struct.args*, align 4
  %buffer = alloca %struct.buffer, align 4
  %mutex = alloca %union.pthread_mutex_t, align 4
  %1 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 0
  store i32 %opt.0, i32* %1, align 4
  %2 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 1
  store i32 %opt.1, i32* %2, align 4
  %3 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 2
  store i32 %opt.2, i32* %3, align 4
  %4 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 3
  store i32 %opt.3, i32* %4, align 4
  %5 = call i32 @pthread_mutex_init(%union.pthread_mutex_t* %mutex, %union.pthread_mutexattr_t* null) #4
  %6 = call i32 @time(i32* null) #4
  call void @srand(i32 %6) #4
  %7 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 1
  %8 = load i32, i32* %7, align 4
  %9 = mul i32 %8, 4
  %10 = call noalias i8* @malloc(i32 %9) #4
  %11 = bitcast i8* %10 to i32*
  %12 = getelementptr inbounds %struct.buffer, %struct.buffer* %buffer, i32 0, i32 0
  store i32* %11, i32** %12, align 4
  %13 = icmp eq i32* %11, null
  br i1 %13, label %14, label %16

; <label>:14                                      ; preds = %0
  %15 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i32 0, i32 0))
  call void @exit(i32 1) #5
  unreachable

; <label>:16                                      ; preds = %0
  %17 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 1
  %18 = load i32, i32* %17, align 4
  %19 = getelementptr inbounds %struct.buffer, %struct.buffer* %buffer, i32 0, i32 1
  store i32 %18, i32* %19, align 4
  store i32 0, i32* %i, align 4
  br label %20

; <label>:20                                      ; preds = %31, %16
  %21 = load i32, i32* %i, align 4
  %22 = getelementptr inbounds %struct.buffer, %struct.buffer* %buffer, i32 0, i32 1
  %23 = load i32, i32* %22, align 4
  %24 = icmp slt i32 %21, %23
  br i1 %24, label %25, label %34

; <label>:25                                      ; preds = %20
  %26 = load i32, i32* %i, align 4
  %27 = load i32, i32* %i, align 4
  %28 = getelementptr inbounds %struct.buffer, %struct.buffer* %buffer, i32 0, i32 0
  %29 = load i32*, i32** %28, align 4
  %30 = getelementptr inbounds i32, i32* %29, i32 %27
  store i32 %26, i32* %30, align 4
  br label %31

; <label>:31                                      ; preds = %25
  %32 = load i32, i32* %i, align 4
  %33 = add nsw i32 %32, 1
  store i32 %33, i32* %i, align 4
  br label %20

; <label>:34                                      ; preds = %20
  %35 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 0
  %36 = load i32, i32* %35, align 4
  %37 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.3, i32 0, i32 0), i32 %36)
  %38 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 0
  %39 = load i32, i32* %38, align 4
  %40 = mul i32 8, %39
  %41 = call noalias i8* @malloc(i32 %40) #4
  %42 = bitcast i8* %41 to %struct.thread_info*
  store %struct.thread_info* %42, %struct.thread_info** %threads, align 4
  %43 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 0
  %44 = load i32, i32* %43, align 4
  %45 = mul i32 20, %44
  %46 = call noalias i8* @malloc(i32 %45) #4
  %47 = bitcast i8* %46 to %struct.args*
  store %struct.args* %47, %struct.args** %args, align 4
  %48 = load %struct.thread_info*, %struct.thread_info** %threads, align 4
  %49 = icmp eq %struct.thread_info* %48, null
  br i1 %49, label %53, label %50

; <label>:50                                      ; preds = %34
  %51 = load %struct.args*, %struct.args** %args, align 4
  %52 = icmp eq %struct.args* %51, null
  br i1 %52, label %53, label %55

; <label>:53                                      ; preds = %50, %34
  %54 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i32 0, i32 0))
  call void @exit(i32 1) #5
  unreachable

; <label>:55                                      ; preds = %50
  %56 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.4, i32 0, i32 0))
  %57 = getelementptr inbounds %struct.buffer, %struct.buffer* %buffer, i32 0, i32 0
  %58 = load i32*, i32** %57, align 4
  %59 = getelementptr inbounds %struct.buffer, %struct.buffer* %buffer, i32 0, i32 1
  %60 = load i32, i32* %59, align 4
  call void @print_buffer(i32* %58, i32 %60)
  store i32 0, i32* %i, align 4
  br label %61

; <label>:61                                      ; preds = %111, %55
  %62 = load i32, i32* %i, align 4
  %63 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 0
  %64 = load i32, i32* %63, align 4
  %65 = icmp slt i32 %62, %64
  br i1 %65, label %66, label %114

; <label>:66                                      ; preds = %61
  %67 = load i32, i32* %i, align 4
  %68 = load i32, i32* %i, align 4
  %69 = load %struct.thread_info*, %struct.thread_info** %threads, align 4
  %70 = getelementptr inbounds %struct.thread_info, %struct.thread_info* %69, i32 %68
  %71 = getelementptr inbounds %struct.thread_info, %struct.thread_info* %70, i32 0, i32 1
  store i32 %67, i32* %71, align 4
  %72 = load i32, i32* %i, align 4
  %73 = load i32, i32* %i, align 4
  %74 = load %struct.args*, %struct.args** %args, align 4
  %75 = getelementptr inbounds %struct.args, %struct.args* %74, i32 %73
  %76 = getelementptr inbounds %struct.args, %struct.args* %75, i32 0, i32 0
  store i32 %72, i32* %76, align 4
  %77 = load i32, i32* %i, align 4
  %78 = load %struct.args*, %struct.args** %args, align 4
  %79 = getelementptr inbounds %struct.args, %struct.args* %78, i32 %77
  %80 = getelementptr inbounds %struct.args, %struct.args* %79, i32 0, i32 3
  store %struct.buffer* %buffer, %struct.buffer** %80, align 4
  %81 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 3
  %82 = load i32, i32* %81, align 4
  %83 = load i32, i32* %i, align 4
  %84 = load %struct.args*, %struct.args** %args, align 4
  %85 = getelementptr inbounds %struct.args, %struct.args* %84, i32 %83
  %86 = getelementptr inbounds %struct.args, %struct.args* %85, i32 0, i32 1
  store i32 %82, i32* %86, align 4
  %87 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 2
  %88 = load i32, i32* %87, align 4
  %89 = load i32, i32* %i, align 4
  %90 = load %struct.args*, %struct.args** %args, align 4
  %91 = getelementptr inbounds %struct.args, %struct.args* %90, i32 %89
  %92 = getelementptr inbounds %struct.args, %struct.args* %91, i32 0, i32 2
  store i32 %88, i32* %92, align 4
  %93 = load i32, i32* %i, align 4
  %94 = load %struct.args*, %struct.args** %args, align 4
  %95 = getelementptr inbounds %struct.args, %struct.args* %94, i32 %93
  %96 = getelementptr inbounds %struct.args, %struct.args* %95, i32 0, i32 4
  store %union.pthread_mutex_t* %mutex, %union.pthread_mutex_t** %96, align 4
  %97 = load i32, i32* %i, align 4
  %98 = load %struct.thread_info*, %struct.thread_info** %threads, align 4
  %99 = getelementptr inbounds %struct.thread_info, %struct.thread_info* %98, i32 %97
  %100 = getelementptr inbounds %struct.thread_info, %struct.thread_info* %99, i32 0, i32 0
  %101 = load i32, i32* %i, align 4
  %102 = load %struct.args*, %struct.args** %args, align 4
  %103 = getelementptr inbounds %struct.args, %struct.args* %102, i32 %101
  %104 = bitcast %struct.args* %103 to i8*
  %105 = call i32 @pthread_create(i32* %100, %union.pthread_attr_t* null, i8* (i8*)* @swap, i8* %104) #4
  %106 = icmp ne i32 0, %105
  br i1 %106, label %107, label %110

; <label>:107                                     ; preds = %66
  %108 = load i32, i32* %i, align 4
  %109 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.5, i32 0, i32 0), i32 %108)
  call void @exit(i32 1) #5
  unreachable

; <label>:110                                     ; preds = %66
  br label %111

; <label>:111                                     ; preds = %110
  %112 = load i32, i32* %i, align 4
  %113 = add nsw i32 %112, 1
  store i32 %113, i32* %i, align 4
  br label %61

; <label>:114                                     ; preds = %61
  store i32 0, i32* %i, align 4
  br label %115

; <label>:115                                     ; preds = %127, %114
  %116 = load i32, i32* %i, align 4
  %117 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 0
  %118 = load i32, i32* %117, align 4
  %119 = icmp slt i32 %116, %118
  br i1 %119, label %120, label %130

; <label>:120                                     ; preds = %115
  %121 = load i32, i32* %i, align 4
  %122 = load %struct.thread_info*, %struct.thread_info** %threads, align 4
  %123 = getelementptr inbounds %struct.thread_info, %struct.thread_info* %122, i32 %121
  %124 = getelementptr inbounds %struct.thread_info, %struct.thread_info* %123, i32 0, i32 0
  %125 = load i32, i32* %124, align 4
  %126 = call i32 @pthread_join(i32 %125, i8** null)
  br label %127

; <label>:127                                     ; preds = %120
  %128 = load i32, i32* %i, align 4
  %129 = add nsw i32 %128, 1
  store i32 %129, i32* %i, align 4
  br label %115

; <label>:130                                     ; preds = %115
  %131 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.6, i32 0, i32 0))
  %132 = getelementptr inbounds %struct.buffer, %struct.buffer* %buffer, i32 0, i32 0
  %133 = load i32*, i32** %132, align 4
  %134 = getelementptr inbounds %struct.buffer, %struct.buffer* %buffer, i32 0, i32 1
  %135 = load i32, i32* %134, align 4
  call void @print_buffer(i32* %133, i32 %135)
  call void @pthread_exit(i8* null) #6
  unreachable
                                                  ; No predecessors!
  ret void
}

; Function Attrs: nounwind
declare i32 @pthread_mutex_init(%union.pthread_mutex_t*, %union.pthread_mutexattr_t*) #0

; Function Attrs: nounwind
declare void @srand(i32) #0

; Function Attrs: nounwind
declare i32 @time(i32*) #0

; Function Attrs: nounwind
declare noalias i8* @malloc(i32) #0

; Function Attrs: noreturn nounwind
declare void @exit(i32) #2

; Function Attrs: nounwind
declare i32 @pthread_create(i32*, %union.pthread_attr_t*, i8* (i8*)*, i8*) #0

declare i32 @pthread_join(i32, i8**) #1

; Function Attrs: noreturn
declare void @pthread_exit(i8*) #3

; Function Attrs: nounwind
define i32 @main(i32 %argc, i8** %argv) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i8**, align 4
  %opt = alloca %struct.options, align 4
  store i32 0, i32* %1, align 4
  store i32 %argc, i32* %2, align 4
  store i8** %argv, i8*** %3, align 4
  %4 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 0
  store i32 10, i32* %4, align 4
  %5 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 1
  store i32 10, i32* %5, align 4
  %6 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 2
  store i32 100, i32* %6, align 4
  %7 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 3
  store i32 0, i32* %7, align 4
  %8 = load i32, i32* %2, align 4
  %9 = load i8**, i8*** %3, align 4
  %10 = call i32 @read_options(i32 %8, i8** %9, %struct.options* %opt)
  %11 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 0
  %12 = load i32, i32* %11, align 4
  %13 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 1
  %14 = load i32, i32* %13, align 4
  %15 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 2
  %16 = load i32, i32* %15, align 4
  %17 = getelementptr inbounds %struct.options, %struct.options* %opt, i32 0, i32 3
  %18 = load i32, i32* %17, align 4
  call void @start_threads(i32 %12, i32 %14, i32 %16, i32 %18)
  call void @exit(i32 0) #5
  unreachable
                                                  ; No predecessors!
  %20 = load i32, i32* %1, align 4
  ret i32 %20
}

declare i32 @read_options(i32, i8**, %struct.options*) #1

attributes #0 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="pentium4" "target-features"="+fxsr,+mmx,+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="pentium4" "target-features"="+fxsr,+mmx,+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="pentium4" "target-features"="+fxsr,+mmx,+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="pentium4" "target-features"="+fxsr,+mmx,+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { noreturn nounwind }
attributes #6 = { noreturn }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.8.0-svn260756-1~exp1 (branches/release_38)"}
