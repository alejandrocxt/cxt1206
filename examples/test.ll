; ModuleID = 'test.c'
target datalayout = "e-m:e-p:32:32-f64:32:64-f80:32-n8:16:32-S128"
target triple = "i686-pc-linux-gnu"

; Function Attrs: nounwind
define i32 @hola() #0 {
  %A = alloca [155 x i32], align 4
  %z = alloca float, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %i = alloca i32, align 4
  store float 2.000000e+00, float* %x, align 4
  store float 5.000000e+00, float* %y, align 4
  %1 = load float, float* %y, align 4
  %2 = load float, float* %x, align 4
  %3 = fadd float %1, %2
  store float %3, float* %z, align 4
  %4 = load float, float* %z, align 4
  %5 = load float, float* %z, align 4
  %6 = fmul float %4, %5
  store float %6, float* %x, align 4
  %7 = load float, float* %y, align 4
  %8 = load float, float* %x, align 4
  %9 = fadd float %7, %8
  store float %9, float* %z, align 4
  %10 = load float, float* %z, align 4
  %11 = load float, float* %z, align 4
  %12 = fmul float %10, %11
  store float %12, float* %x, align 4
  %13 = load float, float* %z, align 4
  %14 = load float, float* %x, align 4
  %15 = fdiv float %13, %14
  store float %15, float* %y, align 4
  store i32 0, i32* %i, align 4
  br label %16

; <label>:16                                      ; preds = %23, %0
  %17 = load i32, i32* %i, align 4
  %18 = icmp slt i32 %17, 155
  br i1 %18, label %19, label %26

; <label>:19                                      ; preds = %16
  %20 = load i32, i32* %i, align 4
  %21 = load i32, i32* %i, align 4
  %22 = getelementptr inbounds [155 x i32], [155 x i32]* %A, i32 0, i32 %21
  store i32 %20, i32* %22, align 4
  br label %23

; <label>:23                                      ; preds = %19
  %24 = load i32, i32* %i, align 4
  %25 = add nsw i32 %24, 2
  store i32 %25, i32* %i, align 4
  br label %16

; <label>:26                                      ; preds = %16
  ret i32 0
}

; Function Attrs: nounwind
define i32 @adios() #0 {
  %A = alloca [155 x i32], align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %z = alloca float, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %i = alloca i32, align 4
  %i1 = alloca i32, align 4
  store i32 0, i32* %a, align 4
  store i32 3, i32* %b, align 4
  store float 2.000000e+00, float* %x, align 4
  store float 5.000000e+00, float* %y, align 4
  %1 = load float, float* %y, align 4
  %2 = load float, float* %x, align 4
  %3 = fadd float %1, %2
  store float %3, float* %z, align 4
  %4 = load float, float* %z, align 4
  %5 = load float, float* %z, align 4
  %6 = fmul float %4, %5
  store float %6, float* %x, align 4
  %7 = load float, float* %y, align 4
  %8 = load float, float* %x, align 4
  %9 = fadd float %7, %8
  store float %9, float* %z, align 4
  %10 = load float, float* %z, align 4
  %11 = load float, float* %z, align 4
  %12 = fmul float %10, %11
  store float %12, float* %x, align 4
  %13 = load float, float* %y, align 4
  %14 = load float, float* %x, align 4
  %15 = fadd float %13, %14
  store float %15, float* %z, align 4
  %16 = load float, float* %z, align 4
  %17 = load float, float* %z, align 4
  %18 = fmul float %16, %17
  store float %18, float* %x, align 4
  %19 = load i32, i32* %b, align 4
  %20 = load i32, i32* %a, align 4
  %21 = add nsw i32 %20, %19
  store i32 %21, i32* %a, align 4
  store i32 0, i32* %i, align 4
  br label %22

; <label>:22                                      ; preds = %29, %0
  %23 = load i32, i32* %i, align 4
  %24 = icmp slt i32 %23, 155
  br i1 %24, label %25, label %32

; <label>:25                                      ; preds = %22
  %26 = load i32, i32* %i, align 4
  %27 = load i32, i32* %i, align 4
  %28 = getelementptr inbounds [155 x i32], [155 x i32]* %A, i32 0, i32 %27
  store i32 %26, i32* %28, align 4
  br label %29

; <label>:29                                      ; preds = %25
  %30 = load i32, i32* %i, align 4
  %31 = add nsw i32 %30, 1
  store i32 %31, i32* %i, align 4
  br label %22

; <label>:32                                      ; preds = %22
  store i32 0, i32* %i1, align 4
  br label %33

; <label>:33                                      ; preds = %40, %32
  %34 = load i32, i32* %i1, align 4
  %35 = icmp slt i32 %34, 155
  br i1 %35, label %36, label %43

; <label>:36                                      ; preds = %33
  %37 = load i32, i32* %i1, align 4
  %38 = load i32, i32* %i1, align 4
  %39 = getelementptr inbounds [155 x i32], [155 x i32]* %A, i32 0, i32 %38
  store i32 %37, i32* %39, align 4
  br label %40

; <label>:40                                      ; preds = %36
  %41 = load i32, i32* %i1, align 4
  %42 = add nsw i32 %41, 1
  store i32 %42, i32* %i1, align 4
  br label %33

; <label>:43                                      ; preds = %33
  ret i32 0
}

; Function Attrs: nounwind
define i32 @main(i32 %argc, i8** %argv) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i8**, align 4
  %A = alloca [155 x i32], align 4
  %a = alloca i32, align 4
  %z = alloca float, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %i = alloca i32, align 4
  %i1 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  store i32 %argc, i32* %2, align 4
  store i8** %argv, i8*** %3, align 4
  store float 2.000000e+00, float* %x, align 4
  store float 5.000000e+00, float* %y, align 4
  %4 = load float, float* %y, align 4
  %5 = load float, float* %x, align 4
  %6 = fadd float %4, %5
  store float %6, float* %z, align 4
  %7 = load float, float* %z, align 4
  %8 = load float, float* %z, align 4
  %9 = fmul float %7, %8
  store float %9, float* %x, align 4
  %10 = load float, float* %z, align 4
  %11 = fpext float %10 to double
  %12 = fadd double %11, 2.000000e+01
  %13 = fptrunc double %12 to float
  store float %13, float* %x, align 4
  store i32 2, i32* %a, align 4
  %14 = load i32, i32* %a, align 4
  %15 = mul nsw i32 %14, 2
  store i32 %15, i32* %a, align 4
  %16 = call i32 @adios()
  store i32 0, i32* %i, align 4
  br label %17

; <label>:17                                      ; preds = %24, %0
  %18 = load i32, i32* %i, align 4
  %19 = icmp slt i32 %18, 155
  br i1 %19, label %20, label %27

; <label>:20                                      ; preds = %17
  %21 = load i32, i32* %i, align 4
  %22 = load i32, i32* %i, align 4
  %23 = getelementptr inbounds [155 x i32], [155 x i32]* %A, i32 0, i32 %22
  store i32 %21, i32* %23, align 4
  br label %24

; <label>:24                                      ; preds = %20
  %25 = load i32, i32* %i, align 4
  %26 = add nsw i32 %25, 1
  store i32 %26, i32* %i, align 4
  br label %17

; <label>:27                                      ; preds = %17
  store i32 0, i32* %i1, align 4
  br label %28

; <label>:28                                      ; preds = %35, %27
  %29 = load i32, i32* %i1, align 4
  %30 = icmp slt i32 %29, 155
  br i1 %30, label %31, label %38

; <label>:31                                      ; preds = %28
  %32 = load i32, i32* %i1, align 4
  %33 = load i32, i32* %i1, align 4
  %34 = getelementptr inbounds [155 x i32], [155 x i32]* %A, i32 0, i32 %33
  store i32 %32, i32* %34, align 4
  br label %35

; <label>:35                                      ; preds = %31
  %36 = load i32, i32* %i1, align 4
  %37 = add nsw i32 %36, 1
  store i32 %37, i32* %i1, align 4
  br label %28

; <label>:38                                      ; preds = %28
  %39 = call i32 @hola()
  ret i32 0
}

attributes #0 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="pentium4" "target-features"="+fxsr,+mmx,+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.8.0-svn260756-1~exp1 (branches/release_38)"}
