#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"

class Cxt001Pass : public llvm::FunctionPass {
public:
  static char ID;

  Cxt001Pass()
      : FunctionPass(ID), totalFunctions(0), totalBlocks(0),
        totalInstructions(0),totalInstructionsPrecision(0) ,totalInstructionsInt(0),totalInstructionsPFloat(0),
	    totalInstructionsPFloatPrecision(0), totalLoops(0) {}

  void getAnalysisUsage(llvm::AnalysisUsage &AU) const override;

  bool runOnFunction(llvm::Function &F) override;

  void print(llvm::raw_ostream &O, const llvm::Module *M) const override;

private:
  int totalFunctions;
  int totalBlocks;
  int totalInstructions;
  int totalInstructionsPrecision;
  int totalInstructionsInt;
  int totalInstructionsPFloat;
  int totalInstructionsPFloatPrecision;
  int totalLoops;
};
