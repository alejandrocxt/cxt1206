#include "Cxt001Pass.h"

#include "llvm/Analysis/LoopPass.h"
#include "llvm/Support/raw_ostream.h"
#include "iostream"
#include "string"
#include "utility"
#include "map"
#include "iomanip"
#include "vector"

using namespace llvm;
using namespace std;

const int UMBRAL = 50;
const int UMBRALP = 100;

typedef pair<const Function*,vector<int>> parDatos;
map<const Function*,vector<int>> mapInfo;

typedef pair<const Function*,vector<const Function*>> parSubFun;
map<const Function*,vector<const Function*>> mapSubFun;

char Cxt001Pass::ID = 0;


void Cxt001Pass::getAnalysisUsage(AnalysisUsage &AU) const {
  // Specifies that the pass will not invalidate any analysis already built on the IR
  AU.setPreservesAll();
  // Specifies that the pass will use the analysis LoopInfo
  AU.addRequiredTransitive<LoopInfoWrapperPass>();
}


void contFloatsCalls(int &totalInstructionsPFloatFunction, int &totalInstructionsFunction){

  map<const Function*,vector<int>>::iterator itMapInfo = mapInfo.begin();
  map<const Function*,vector<const Function*>>::iterator itMapSubFun = mapSubFun.begin();

  while (itMapSubFun != mapSubFun.end()){
    if (!itMapSubFun->second.empty()){
      int pos = 0;
      int numHijos = itMapSubFun->second.size();
      while(pos < numHijos){
        itMapInfo = mapInfo.find(itMapSubFun->second[pos]);
        if (itMapInfo != mapInfo.end()){ 
          totalInstructionsPFloatFunction += itMapInfo->second[1];
          totalInstructionsFunction += itMapInfo->second[3];
        }  
        pos++;
      }
    }
    itMapSubFun++;
  }
}


bool Cxt001Pass::runOnFunction(Function &F) {
  LoopInfoWrapperPass &LIWP = getAnalysis<LoopInfoWrapperPass>();
  LoopInfo &LI = LIWP.getLoopInfo();
  vector<int> datos(5);
  vector<const Function*> vectorSubFun = {};
  int totalInstructionsIntFunction = 0;
  int totalInstructionsPFloatFunction = 0;
  int totalInstructionsFunction = 0; 

  /*for (Loop *L : LI) {
    totalLoops++;    
  } */

  for (BasicBlock &BB : F) {
	  for (Instruction &I : BB) {
	  	totalInstructions++;
      totalInstructionsFunction++;
	  	switch (I.getOpcode()){
		   	case Instruction::Add: {
            totalInstructionsInt++;
            totalInstructionsIntFunction++;
          }
					break;
			  case Instruction::FAdd:{ 
            totalInstructionsPFloat++;
            totalInstructionsPFloatFunction++;
		      }
          break;
		    case Instruction::Sub: {
            totalInstructionsInt++;
            totalInstructionsIntFunction++;
          }
					break;
			  case Instruction::FSub: { 
            totalInstructionsPFloat++;
            totalInstructionsPFloatFunction++;
          }
		      break;
		 	  case Instruction::Mul: {
            totalInstructionsInt++;
            totalInstructionsIntFunction++;
          }
		     	break;
			  case Instruction::FMul: { 
            totalInstructionsPFloat++;
            totalInstructionsPFloatFunction++;
          }
	        break;
	 		  case Instruction::UDiv: {
            totalInstructionsInt++;
            totalInstructionsIntFunction++;
          }
          break;
			  case Instruction::SDiv: {
            totalInstructionsInt++;
            totalInstructionsIntFunction++;
          }
          break;
			  case Instruction::FDiv: { 
            totalInstructionsPFloat++;
            totalInstructionsPFloatFunction++;
          }
          break;
        case Instruction::SRem: {
            totalInstructionsInt++;
            totalInstructionsIntFunction++;
          }
          break;
        case Instruction::URem: {
            totalInstructionsInt++;
            totalInstructionsIntFunction++;
          }
          break; 
        case Instruction::FRem: { 
            totalInstructionsPFloat++;
            totalInstructionsPFloatFunction++;
          }
          break; 
        case llvm::Instruction::Call:
            if (const CallInst *CI = dyn_cast<CallInst>(&I)){
              vectorSubFun.push_back(CI->getCalledFunction());
            }
          break;                          
		} 
	}
    totalBlocks++;
  }
  totalFunctions++;

  mapSubFun.insert(parSubFun((const Function *)&F,vectorSubFun));

  datos[1] = totalInstructionsPFloatFunction;  //FLOPS
  datos[2] = totalInstructionsIntFunction;  //INTOPS
  datos[3] = totalInstructionsFunction;     // Instrucciones por función SIN precision

  contFloatsCalls(totalInstructionsPFloatFunction, totalInstructionsFunction);

  datos[0] = totalInstructionsPFloatFunction; //LOCS
  datos[4] = totalInstructionsFunction;     // Instrucciones por función CON precision
  
  mapInfo.insert(parDatos((const Function *)&F,datos));

  totalInstructionsPFloatPrecision += totalInstructionsPFloatFunction; //Guardamos FLOPS con precision en la var global
  totalInstructionsPrecision += totalInstructionsFunction; //Guardamos las instrucciones con precision en la var global

  //totalInstructions
  //totalInstructionsInt
  //totalInstructionsPFloat
  return false;
}



void Cxt001Pass::print(raw_ostream &O, const Module *M) const {
  O << "\nFor module: " << M->getName() << "\n\n";
  //O << "Number of loops: " << totalLoops << "\n";
  //O << "Number of blocks: " << totalBlocks << "\n";
  map<const Function*,vector<int>>::iterator iteratorMapInfo = mapInfo.begin();

  O << "--------------------------------------------------------------------\n";
  O << "|Total program instrunctions without precision | "<<  totalInstructions << " instrunctions \n";
  O << "--------------------------------------------------------------------\n";
  O << "|Total program instrunctions with precision    | "<<  totalInstructionsPrecision << " instrunctions \n";
  O << "--------------------------------------------------------------------\n";
  O << "|Total program Integer OPS:                    | "<<  totalInstructionsInt << " INTOPS          \n";
  O << "--------------------------------------------------------------------\n";
  O << "|Total program Float OPS without precision:    | "<<  totalInstructionsPFloat << " FLOPS           \n";
  O << "--------------------------------------------------------------------\n";
  O << "|Total program Float OPS with precision:       | "<<  totalInstructionsPFloatPrecision << " FLOPS           \n";
  O << "--------------------------------------------------------------------\n\n\n";

  O << "Umbral without:  "<< UMBRAL << " instrunctions \nUmbral with:  " << UMBRALP<<" instrunctions\n\n";

  O << "--------------------------------------------------------------------------------------------\n";
  O << "Function        |  LOCS    |   FLOPS    |    INTOPS   |  Intruct without  |  Instruct with |\n";
  O << "--------------------------------------------------------------------------------------------\n";
  while (iteratorMapInfo != mapInfo.end()){	
	O << iteratorMapInfo->first->getName() <<":	        " << iteratorMapInfo->second[0] << " LOCS	  ";
    O << iteratorMapInfo->second[1] << " FLOPS      ";
    O << iteratorMapInfo->second[2] << " INTOPS    ";
    if ((iteratorMapInfo->second[3]) > UMBRAL){
			O << iteratorMapInfo->second[3] << " instruc **  ";
		} else {
			O << iteratorMapInfo->second[3] << " instruc     ";
		}
     if ((iteratorMapInfo->second[4]) > UMBRALP){
      O << iteratorMapInfo->second[4] << " instruc_precis **  \n";
    } else {
      O << iteratorMapInfo->second[4] << " instruc_precis     \n";
    }
    iteratorMapInfo++;
  }
  O << "--------------------------------------------------------------------------------------------\n";

}
